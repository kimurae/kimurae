## Kimuræ 👋

> Just a salamander that likes to code from his warm rock.


### 🧰 Toolbox

<p align="left">
  <img height="30" width="30" alt="Docker" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-plain.svg" />
  <img height="30" width="30" alt="Elixir Logo" src="https://github.com/devicons/devicon/raw/master/icons/elixir/elixir-plain.svg" />
  <img height="30" width="30" alt="Phoenix" src="https://github.com/devicons/devicon/raw/master/icons/phoenix/phoenix-original.svg" />

  <img height="30" width="30" alt="Golang" src="https://github.com/devicons/devicon/raw/master/icons/go/go-plain.svg" />

  <img height="30" width="30" alt="Javascript" src="https://github.com/devicons/devicon/raw/master/icons/javascript/javascript-plain.svg" />
  <img height="30" width="30" alt="NestJS" src="https://github.com/devicons/devicon/raw/master/icons/nestjs/nestjs-plain.svg" />
  <img height="30" width="30" alt="React" src="https://github.com/devicons/devicon/raw/master/icons/react/react-original.svg" />

  <img height="30" width="30" alt="LaTeX" src="https://github.com/devicons/devicon/raw/master/icons/latex/latex-original.svg" />

  <img height="30" width="30" alt="Perl" src="https://github.com/devicons/devicon/raw/master/icons/perl/perl-original.svg" />

  <img height="30" width="30" alt="Python" src="https://github.com/devicons/devicon/raw/master/icons/python/python-original.svg" />

  <img height="30" width="30" alt="Ruby" src="https://github.com/devicons/devicon/raw/master/icons/ruby/ruby-original.svg" />
  <img height="30" width="30" alt="Ruby on Rails" src="https://github.com/devicons/devicon/raw/master/icons/rails/rails-plain.svg" />

  <img height="30" width="30" alt="Rust" src="https://github.com/devicons/devicon/raw/master/icons/rust/rust-plain.svg" />
               
  <img height="30" width="30" alt="MySQL" src="https://github.com/devicons/devicon/raw/master/icons/mysql/mysql-original.svg" />
  <img height="30" width="30" alt="Postgres" src="https://github.com/devicons/devicon/raw/master/icons/postgresql/postgresql-plain.svg" />

  <img height="30" width="30" alt="Vim" src="https://github.com/devicons/devicon/raw/master/icons/vim/vim-plain.svg" />
</p>

### 🐻Toybox

<p align="left">
  <img height="50" width="50" alt="Board Games" src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Dice.svg/2560px-Dice.svg.png" />
  <img height="50" width="50" alt="Fantasy" src="https://upload.wikimedia.org/wikipedia/commons/8/8a/Speculative_fiction_portal_logo_small_transparent.png" />
  <img height="50" width="50" alt="Hiking" src="https://freesvg.org/img/johnny-automatic-NPS-map-pictographs-part-95.png" />
  <img height="50" width="50" alt="Photography" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/High-contrast-camera-photo.svg/2048px-High-contrast-camera-photo.svg.png" />
  <img height="50" width="50" alt="Tabletop RPG" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Font_Awesome_5_solid_dice-d20.svg/512px-Font_Awesome_5_solid_dice-d20.svg.png?20181017203656" />
  <img height="50" width="50" alt="Video Games" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Game_controller.svg/2048px-Game_controller.svg.png" />
</p>
